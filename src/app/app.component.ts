import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Pipes';

  nombre = 'Aarón';
  PI = Math.PI;

  texto = "HoLA esTO eS uN teXtO";
}
